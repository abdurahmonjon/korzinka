package main

import (
	"korzinka/config"
	"korzinka/postgres"
	"korzinka/server"
	"korzinka/service"
	"log"
)

func main() {
	conf, err := config.Load()
	if err != nil {
		log.Panic("Failed configuration")
	}
	db, err := postgres.Connect(conf)
	if err != nil {
		log.Panic("failed connection to postgres")
	}
	p := postgres.New(db)
	s := service.New(p)
	server.NewHandler(s)
}
