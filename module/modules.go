package module

import (
	"github.com/google/uuid"
	"time"
)

type Costumer struct {
	Id         string
	Name       string
	UserName   string
	Password   string
	Money      int
	Address    string
	JoinedTime time.Time
}

func NewCostumer() Costumer {
	return Costumer{
		Id:         uuid.NewString(),
		JoinedTime: time.Now(),
	}
}

type Seller struct {
	Id         string
	Name       string
	UserName   string
	Password   string
	Products   []string
	JoinedTime time.Time
}

func NewSeller() Seller {
	return Seller{
		Id:         uuid.NewString(),
		JoinedTime: time.Now(),
	}
}

type Category struct {
	Id   string
	Name string
}

type Product struct {
	Id         string
	Name       string
	CategoryId string
	SellerID   string
	Price      int
	AddedTime  time.Time
	Status     bool
}

func NewProduct() Product {
	return Product{
		Id:        uuid.NewString(),
		AddedTime: time.Now(),
		Status:    true,
	}
}

type Order struct {
	Id         string
	ProductId  string
	CostumerID string
	Date       time.Time
}

func NewOrder() Order {
	return Order{
		Id:   uuid.NewString(),
		Date: time.Now(),
	}
}

type Payment struct {
	Id         string
	ProductId  string
	CostumerId string
	Status     bool
}

func NewPayment() Payment {
	return Payment{
		Id:     uuid.NewString(),
		Status: false,
	}
}

type Transaction struct {
	Id         string
	OrderId    string
	CostumerId string
	SellerID   string
	PaymentId  string
}

func NewTransaction() Transaction {
	return Transaction{
		Id: uuid.NewString(),
	}
}
