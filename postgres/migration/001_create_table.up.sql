create table costumers (
    id uuid primary key ,
    name varchar(50)
    username varchar(35) unique ,
    password varchar(30),
    money int,
    address varchar(100),
    joined_time timestamp);

create table sellers (
    id uuid primary key ,
    name varchar(50),
    username varchar(35) unique ,
    password varchar(30),
    products uuid[])
    joined_time timestamp;

create table categories (
    id uuid primary key ,
    name varchar(50));

create table products (
    id uuid primary key ,
    name varchar(30),
    category_id uuid references categories(id),
    seller_id uuid references sellers(id),
    price int
    added_time timestamp,
    status boolean default false);

create table orders (
    id uuid primary key ,
    product_id uuid references products(id),
    costumer_id uuid references costumers(id),
    date timestamp);

create table payments (
    id uuid primary key ,
    product_id uuid references products(id),
    costumer_id uuid references costumers(id),
    status boolean default false);

create table transactions (
    id uuid primary key ,
    costumer_id uuid references costumers(id),
    product_id uuid references products(id),
    order_id uuid references order(id),
    payment_id uuid references payment(id));
