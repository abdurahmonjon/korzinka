package postgres

import (
	"context"
	"database/sql"
	"korzinka/module"
)

type Repository struct {
	DB *sql.DB
}

func New(db *sql.DB) Repository {
	return Repository{
		db,
	}
}

func (r Repository) Login(ctx context.Context, username, password string) (module.Costumer, error) {
	var newUser module.Costumer
	row, err := r.DB.Query("select * from costumers where username = $1 and password = $2", username, password)
	if err != nil {
		return module.Costumer{}, err
	}
	for row.Next() {
		row.Scan(&newUser.Id, &newUser.Name, &newUser.UserName, &newUser.Password, &newUser.Money, &newUser.Address, &newUser.JoinedTime)
	}
	return newUser, nil
}
func (r Repository) LogUp(ctx context.Context, costumer module.Costumer) error {
	costumer = module.NewCostumer()
	_, err := r.DB.Exec("insert into costumers values($1,$2,$3,$4,$5,%6,%7,%8)", costumer.Id, costumer.Name, costumer.UserName, costumer.Password, costumer.Money, costumer.Address, costumer.JoinedTime)
	if err != nil {
		return err
	}
	return nil
}
func (r Repository) SearchProduct(ctx context.Context, productName string) ([]module.Product, error) {
	var products []module.Product
	row, err := r.DB.Query("select * from products where name = $1", productName)
	if err != nil {
		return nil, err
	}
	for row.Next() {
		var product module.Product
		row.Scan(&product.Id, &product.Name, &product.CategoryId, &product.SellerID, &product.Price, &product.AddedTime, &product.Status)
		products = append(products, product)
	}
	return products, nil
}
func (r Repository) Categories(ctx context.Context) ([]module.Category, error) {
	var categories []module.Category
	row, err := r.DB.Query("select * from categories")
	if err != nil {
		return nil, err
	}
	for row.Next() {
		var category module.Category
		row.Scan(&category.Id, &category.Name)
		categories = append(categories, category)
	}
	return categories, nil
}
func (r Repository) Order(ctx context.Context, order module.Order) error {
	order = module.NewOrder()
	_, err := r.DB.Exec("insert into orders values($1,$2,$3,$4)", order.Id, order.CostumerID, order.ProductId, order.Date)
	if err != nil {
		return err
	}
	return nil
}
func (r Repository) Payment(ctx context.Context, payment module.Payment) error {
	payment = module.NewPayment()
	_, err := r.DB.Exec("insert into payments values($1,$2,$3,$4)", payment.Id, payment.ProductId, payment.CostumerId, payment.Status)
	if err != nil {
		return err
	}
	return nil
}
func (r Repository) LoginSeller(ctx context.Context, username, password string) (module.Seller, error) {
	var seller module.Seller
	row, err := r.DB.Query("select * from sellers where username = $1 and password=$2", username, password)
	if err != nil {
		return seller, err
	}
	for row.Next() {
		row.Scan(&seller.Id, &seller.Name, &seller.UserName, &seller.Password, &seller.Products, &seller.JoinedTime)
	}
	return seller, nil
}
func (r Repository) LogUpSeller(ctx context.Context, seller module.Seller) error {
	seller = module.NewSeller()
	_, err := r.DB.Exec("insert into sellers values", seller.Id, seller.Name, seller.UserName, seller.Password, seller.Products, seller.JoinedTime)
	if err != nil {
		return err
	}
	return nil
}
func (r Repository) NewProduct(ctx context.Context, product module.Product) error {
	product = module.NewProduct()
	_, err := r.DB.Exec("insert into products values", product.Id, product.Name, product.CategoryId, product.SellerID, product.Price, product.Status, product.AddedTime)
	if err != nil {
		return err
	}
	return nil
}
