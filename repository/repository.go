package repository

import (
	"context"
	"korzinka/module"
)

type Repository interface {
	Login(ctx context.Context, username, password string) (module.Costumer, error)
	LogUp(ctx context.Context, costumer module.Costumer) error
	SearchProduct(ctx context.Context, productName string) ([]module.Product, error)
	Categories(ctx context.Context) ([]module.Category, error)
	Order(ctx context.Context, order module.Order) error
	Payment(ctx context.Context, payment module.Payment) error
	LoginSeller(ctx context.Context, username, password string) (module.Seller, error)
	LogUpSeller(ctx context.Context, seller module.Seller) error
	NewProduct(ctx context.Context, product module.Product) error
}
