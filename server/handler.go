package server

import (
	"github.com/gin-gonic/gin"
	"korzinka/module"
	"korzinka/repository"
	"net/http"
)

type Handler struct {
	Repo repository.Repository
}

func NewHandler(repository2 repository.Repository) Handler {
	return Handler{
		Repo: repository2,
	}
}

// LogIn
// @Summary      LogIn
// @Description  Login costumer
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        module.costumer body
// @Success      200 {object}
// @Failure      400 {object} error
// @Failure      500 {object} error
// @Router       /costumer/login [post]
func (h Handler) LogIn(e *gin.Context) {
	var costumer module.Costumer
	if err := e.ShouldBindJSON(&costumer); err != nil {
		e.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}
	user, err := h.Repo.Login(e.Request.Context(), costumer.UserName, costumer.Password)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	e.JSON(http.StatusOK, user)

}
func (h Handler) LogUp(e *gin.Context) {
	var costumer module.Costumer
	if err := e.ShouldBindJSON(&costumer); err != nil {
		e.JSON(http.StatusBadRequest, gin.H{
			"err ": err,
		})
		return
	}
	err := h.Repo.LogUp(e.Request.Context(), costumer)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	e.JSON(http.StatusOK, "successful")
}
func (h Handler) SearchProduct(e *gin.Context) {
	productName := e.Query("product name")
	var products []module.Product
	products, err := h.Repo.SearchProduct(e.Request.Context(), productName)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	e.JSON(http.StatusOK, products)

}
func (h Handler) Categories(e *gin.Context) {
	categories, err := h.Repo.Categories(e.Request.Context())
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	e.JSON(http.StatusOK, categories)

}

func (h Handler) Order(e *gin.Context) {
	var order module.Order
	if err := e.ShouldBindJSON(&order); err != nil {
		e.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
		return
	}
	err := h.Repo.Order(e.Request.Context(), order)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	e.JSON(http.StatusOK, "successful")
}
func (h Handler) Payment(e *gin.Context) {
	var payment module.Payment
	if err := e.ShouldBindJSON(&payment); err != nil {
		e.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
		return
	}
	err := h.Repo.Payment(e.Request.Context(), payment)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"Error": err,
		})
		return
	}
	e.JSON(http.StatusOK, "Successful")
}
func (h Handler) LoginSeller(e *gin.Context) {
	username := e.Query("username")
	password := e.Query("password")
	seller, err := h.Repo.LoginSeller(e.Request.Context(), username, password)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"Error": err,
		})
		return
	}
	e.JSON(http.StatusOK, seller)
}
func (h Handler) LogUpSeller(e *gin.Context) {
	var seller module.Seller
	if err := e.ShouldBindJSON(&seller); err != nil {
		e.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}
	err := h.Repo.LogUpSeller(e.Request.Context(), seller)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"Error": err,
		})
		return
	}

}
func (h Handler) NewProduct(e *gin.Context) {
	var product module.Product
	if err := e.ShouldBindJSON(&product); err != nil {
		e.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
		return
	}
	err := h.Repo.NewProduct(e.Request.Context(), product)
	if err != nil {
		e.JSON(http.StatusInternalServerError, gin.H{
			"Error": err,
		})
		return
	}
}
