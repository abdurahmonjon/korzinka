package server

import "github.com/gin-gonic/gin"

// @title           Online Ordering
// @version         1.0
// @description     This is a new online ordering app with go.

// Server @host      localhost:8080
type Server interface {
	LogIn(e *gin.Context)
	LogUp(e *gin.Context)
	SearchProduct(e *gin.Context)
	Categories(e *gin.Context)
	Order(e *gin.Context)
	Payment(e *gin.Context)
	LoginSeller(e *gin.Context)
	LogUpSeller(e *gin.Context)
	NewProduct(e *gin.Context)
}

func HandlerRequest(s Server) gin.Engine {
	e := gin.Default()
	c := e.Group("/costumer")
	c.POST("/login", s.LogIn)
	c.POST("log-up", s.LogUp)
	c.GET("/search", s.SearchProduct)
	c.GET("/categories", s.Categories)
	c.POST("/order", s.Order)
	c.PUT("/payment", s.Payment)
	a := e.Group("/seller")
	a.PUT("/login-seller", s.LoginSeller)
	a.POST("/log-up-seller", s.LogUpSeller)
	a.POST("/new-product", s.NewProduct)
	return *e
}
