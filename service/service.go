package service

import (
	"context"
	"errors"
	"korzinka/module"
	"korzinka/repository"
)

type Service struct {
	repo repository.Repository
}

func New(repository2 repository.Repository) *Service {
	return &Service{
		repo: repository2,
	}
}

var (
	userUpErr         = errors.New("not enough arguments ")
	userNamePassErr   = errors.New("Username or password incorrect ")
	noProduct         = errors.New("No products found ")
	noCategory        = errors.New("No category found ")
	invalidOrder      = errors.New("Invalid order request ")
	invalidPayment    = errors.New("Invalid payment ")
	sellerNamePassErr = errors.New("Username or password incorrect ")
	sellerUpErr       = errors.New("Not enough arguments ")
	newProductErr     = errors.New("No enough elements to add product ")
)

func (s Service) Login(ctx context.Context, username, password string) (module.Costumer, error) {
	var costumer module.Costumer
	costumer, err := s.repo.Login(ctx, username, password)
	if err != nil {
		return module.Costumer{}, userNamePassErr
	}
	return costumer, nil
}

func (s Service) LogUp(ctx context.Context, costumer module.Costumer) error {
	err := s.repo.LogUp(ctx, costumer)
	if err != nil {
		return userUpErr
	}
	return nil
}

func (s Service) SearchProduct(ctx context.Context, productName string) ([]module.Product, error) {
	var products []module.Product
	products, err := s.repo.SearchProduct(ctx, productName)
	if err != nil {
		return nil, noProduct
	}
	return products, nil
}

func (s Service) Categories(ctx context.Context) ([]module.Category, error) {
	var categories []module.Category
	categories, err := s.repo.Categories(ctx)
	if err != nil {
		return nil, noCategory
	}
	return categories, nil
}

func (s Service) Order(ctx context.Context, order module.Order) error {

	err := s.repo.Order(ctx, order)
	if err != nil {
		return invalidOrder
	}
	return nil
}

func (s Service) Payment(ctx context.Context, payment module.Payment) error {
	err := s.repo.Payment(ctx, payment)
	if err != nil {
		return invalidPayment
	}
	return nil
}
func (s Service) LoginSeller(ctx context.Context, username, password string) (module.Seller, error) {
	var seller module.Seller
	seller, err := s.repo.LoginSeller(ctx, username, password)
	if err != nil {
		return module.Seller{}, sellerNamePassErr
	}
	return seller, nil
}

func (s Service) LogUpSeller(ctx context.Context, seller module.Seller) error {
	err := s.repo.LogUpSeller(ctx, seller)
	if err != nil {
		return sellerUpErr
	}
	return nil
}
func (s Service) NewProduct(ctx context.Context, product module.Product) error {
	err := s.repo.NewProduct(ctx, product)
	if err != nil {
		return noProduct
	}
	return nil
}

//
//type Repo interface {
//	Login(ctx context.Context, username, password string) (module.Costumer, error)
//	LogUp(ctx context.Context, costumer module.Costumer) error
//	SearchProduct(ctx context.Context, productName string) ([]module.Product, error)
//	Categories(ctx context.Context) ([]module.Category, error)
//	Order(ctx context.Context, order module.Order) error
//	Payment(ctx context.Context, payment module.Payment) error
//	LoginSeller(ctx context.Context, username, password string) (module.Seller, error)
//	LogUpSeller(ctx context.Context, seller module.Seller) error
//	NewProduct(ctx context.Context, product module.Product) error
//}
